#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mastik/low.h>
#include <mastik/util.h>
#include <unistd.h>
#include "channel.h"

//returns mean of tab 
int moyenne(uint64_t tab[],int len)
{
	int s=0;
	for(int i =0;i<len;i++)
	{
		s+= tab[i];
	}
	return s/len;
}

//wait for an N sized rdtscp buffer overflow
//to synchronize sender and receiver
void synchro(int N)
{
	uint64_t mask = (1LL << N) -1;
	uint64_t t = 0;
	do { t= rdtscp64() & mask; } while(t > 1000);
}

//print tab values
void affiche_tab(int tab[],int size)
{
	for(int i = 0;i<size;i++)
	{
		printf("%d ",tab[i]);
	}
	printf("\n");
}

//print binary values of tab
void affiche_tab_bin(char tab[],int size)
{
	for(int i = 0;i<size;i++)
	{
		printf("octet n°%d: ",i);
		for(int j=7;j>=0;j--)
		{
			printf("%d ",tab[i] & (1<<j)?1:0);
		}
		printf(" : %d\n",tab[i]);
	}
	printf("\n");
}

//receive a byte by monitoring access time on covert channel's memory adress
void receive_byte(char *byte,void *mem_addr)
{
	int c;
	uint64_t t = 0;
	uint64_t time[MAX_MEASURE_BIT];
	for(int j=0;j<BYTE_SIZE;j++)
    { 
		synchro(SYNC_BIT);
		c=0;
		t = rdtscp64();
		do
		{
			time[c++]=memaccesstime(mem_addr);
		}
		while(rdtscp64() - t < CYCLE_WAIT_BIT_RECV);
		if(moyenne(time,c) > 150) *byte |= (1 << j);
		else *byte &= ~(1 << j);
	}
}

//receive a packet of bytes
void receive_data(char data[],int size,void *mem_addr)
{
	for(int i=0;i<size;i++)
	{
		synchro(SYNC_BYTE);
		receive_byte(data+i,mem_addr);
	}
}

//send a byte using flush or reload on covert channel's memory adress
void send_byte(char byte,void *mem_addr)
{
	uint64_t t = 0;
	for(int j =0;j<BYTE_SIZE;j++)
	{
		synchro(SYNC_BIT);
		if(byte & (1<<j))
		{
			//send 1
			t = rdtscp64();
			do
			{
				clflush(mem_addr);
			}
			while(rdtscp64() - t < CYCLE_WAIT_BIT_SEND);	
		}
		else
		{	
			//send 0
			t = rdtscp64();
			do
			{
				memaccess(mem_addr);	
			}
			while(rdtscp64() - t < CYCLE_WAIT_BIT_SEND);	
		}
	}
}

//send a packet of bites
void send_data(char data[],int size,void *mem_addr)
{
	for(int i =0;i<size;i++)
	{
		synchro(SYNC_BYTE);
		send_byte(data[i],mem_addr);
	}
}

//decode the 3-bit repetition from buf[] and store the correct data in data[]
int decode_data(char data[],char buf[],int size)
{
	int sum, unknown = 0;
	for(int i=0;i<size;i++)
	{
		for(int j=0;j<BYTE_SIZE;j++)
		{
			sum = 0;
			sum += (buf[i*3] & (1<<j)) ? 1 : 0;
			sum += (buf[i*3+1] & (1<<j)) ? 1 : 0;
			sum += (buf[i*3+2] & (1<<j)) ? 1 : 0;
			switch(sum)
			{
				case 0:
				case 1:
					data[i] &= ~(1<<j);
					break;
				case 2:
				case 3:
					data[i] |= (1<<j);
			}
		}
		if(data[i]<= 'A' || data[i] >= 'Z')
		{
			//data[i] = '+';
			unknown++;
		}
	}
	return unknown;
}

//encode the data from buf[] with a 3-bit repetition and store it in data[]
void encode_data(char data[], char buf[], int size)
{
	for(int i=0;i<size;i++)
	{
		for(int k=0;k<3;k++)
		{
			for(int j=0;j<BYTE_SIZE;j++)
			{
				if(buf[i]&(1<<j))
				{
					data[i*3+k] |= (1<<j);
				}
				else
				{
					data[i*3+k] &= ~(1<<j);
				}
			}
		}
	}
}