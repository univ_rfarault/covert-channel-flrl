LIBMASTIK = -I./Mastik/include -L./Mastik/lib -l:libmastik.a

all: channel send recv 

channel:
	gcc -Wall -Wextra -g -O2 -c channel.c -o channel.o $(LIBMASTIK)

send: channel.o
	gcc -Wall -Wextra -g -O2 send.c $^ -o send $(LIBMASTIK)

recv: channel.o
	gcc -Wall -Wextra -g -O2 recv.c $^ -o recv $(LIBMASTIK)

clean:
	rm send recv channel.o
