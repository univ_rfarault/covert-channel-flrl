#define FILENAME_SEND "channel"
#define OFFSET 0

#define MAX_MEASURE_BIT 10000
#define PACKET_DATA_SIZE 50
#define BYTE_SIZE 8
#define CYCLE_WAIT_BIT_RECV 1000
#define CYCLE_WAIT_BIT_SEND 3000
#define SYNC_START 34
#define SYNC_PACKET 22
#define SYNC_BYTE 20
#define SYNC_BIT 16
#define MAX_PACKET 200

#include <stdint.h>


typedef struct
{
    int p_id;
    char data[PACKET_DATA_SIZE];
}
packet_s;

int moyenne(uint64_t tab[],int len);
void synchro(int N);
void affiche_tab(int tab[],int size);
void affiche_tab_bin(char tab[],int size);
void receive_byte(char *byte,void *mem_addr);
void receive_data(char data[],int size,void *mem_addr);
void send_byte(char byte,void *mem_addr);
void send_data(char data[],int size,void *mem_addr);
void encode_data(char data[], char buf[], int size);
int decode_data(char data[],char buf[],int size);
