#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mastik/low.h>
#include <mastik/util.h>
#include <unistd.h>
#include "channel.h"

int main(int argc, char **argv) 
{
	FILE * file = fopen("test","rb");
	if(file == NULL)
	{
		perror("send file: fopen:");
		return EXIT_FAILURE;
	}

	//create covert channel
	void * mem_addr_send = map_offset(FILENAME_SEND,OFFSET); 
	if(mem_addr_send == NULL){ perror("map_offset: send addr=NULL"); exit(EXIT_FAILURE); } 
	void * mem_addr_recv = map_offset(FILENAME_RECV,OFFSET); 
	if(mem_addr_recv == NULL){ perror("map_offset: recv addr=NULL"); exit(EXIT_FAILURE); } 

	//storage and data buffer
	char buf[PACKET_DATA_SIZE*3];
	char data[MAX_PACKET][PACKET_DATA_SIZE+1];

	//start communication
	synchro(SYNC_START);
	uint64_t start = rdtscp64();
	for(int i=0;i<MAX_PACKET;i++)
	{
		receive_data(buf,PACKET_DATA_SIZE*3,mem_addr_send);
		decode_data(data[i],buf,PACKET_DATA_SIZE);		//3-bit repetition decoding
		data[i][PACKET_DATA_SIZE] = '\0';
		synchro(SYNC_PACKET);
	}
	
	//monitor communication time
	uint64_t end = rdtscp64();
	long double time = (double) (end-start)/(500000000);
	printf("\n Envoi de %d octets en %ld cycles soit environ %.3Lf secondes\nDebit moyen: %.3Lf bits/s\nDonnées:\n\n",MAX_PACKET*PACKET_DATA_SIZE,end-start,time,MAX_PACKET*PACKET_DATA_SIZE/time);

	//print data received
	for(int i=0;i<MAX_PACKET;i++)
	{	
		printf("%d: ",i);
		for(int j=0;j<PACKET_DATA_SIZE;j++)
		{
			printf("%c",data[i][j]);
		}
		printf("\n");
	}
	
	

  	return 0;	
}







